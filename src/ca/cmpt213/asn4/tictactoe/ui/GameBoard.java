package ca.cmpt213.asn4.tictactoe.ui;

import ca.cmpt213.asn4.tictactoe.game.GameLogic;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * The GameBoard class makes the ui for the game grid
 * the grid uses imageViews to show the square, cross and circle
 * @author aryanarora
 */

public class GameBoard extends Application{

    public final static int numRows = 4;
    public final static int numColumns = 4;

    public static Label label;
    public static Button newGameButton;
    public static ImageView gridImageView[][] = new ImageView[numRows][numColumns];


    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Tic Tac Toe");

        //create image views and the array to avoid duplicate children added to the pane
        gridImageView = generateImageViews();

        GridPane gridpane = new GridPane();
        //add Reset(new Game Button at the bottom)
        GridPane bottomInfoPane = addNewGameButton();
        VBox vBox = new VBox(gridpane,bottomInfoPane);


        //make grid lines visible
        gridpane.setGridLinesVisible(true);
        gridpane.setAlignment(Pos.CENTER );
        bottomInfoPane.setAlignment(Pos.CENTER );
        vBox.setSpacing(30.0);

        //add all squares to the grid
        addSquaresToTheGrid(gridpane, gridImageView);


        //create scene
        Scene scene = new Scene(vBox);
        //add scene to the stage
        primaryStage.setScene(scene);
        //show the completed, setup stage
        primaryStage.show();

        GameLogic gameLogic = new GameLogic();
        gameLogic.playGame();

    }

    private GridPane addNewGameButton() {
        label = new Label("X's turn");
        newGameButton = new Button("New Game");

        GridPane gridPane = new GridPane();
        gridPane.setVgap(20);
        gridPane.addRow(0,label);
        gridPane.addRow(1,newGameButton);

        gridPane.setAlignment(Pos.BOTTOM_CENTER);



        return gridPane;

    }

    private ImageView[][] generateImageViews() {
        //to avoid duplicate children added to the gridpane

        for(int row = 0; row < numRows; row++){
            for(int col = 0; col < numColumns; col++){
                Image image = new Image("/imageFiles/square.png");
                ImageView imageView = new ImageView(image);


                imageView.setFitWidth(200);
                imageView.setFitHeight(200);

                gridImageView[row][col] = imageView;
            }
        }

        return gridImageView;
    }

    private void addSquaresToTheGrid(GridPane gridpane, ImageView[][] gridImageView) {
        for(int row = 0; row < numRows; row++){
            for(int column = 0;  column < numColumns; column++){
                gridpane.add(gridImageView[row][column],row,column);
            }
        }
    }

    public static void changeLabel(String s) {
        label.setText(s);
    }

    public static void changeImage(String turn, int row, int col) {
        if(turn == "X's turn"){
            gridImageView[row][col].setImage(new Image("/imageFiles/cross.png"));
        }
        else{
            gridImageView[row][col].setImage(new Image("/imageFiles/circle.png"));
        }
    }

    public static void mains(String[] args) {
        launch(args);
    }

}