package ca.cmpt213.asn4.tictactoe.game;

import ca.cmpt213.asn4.tictactoe.ui.GameBoard;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * The Game logic class runs the actual game and handles all the logic
 * and user input to decided who won or if the game was a draw.
 * @author aryanarora
 */

public class GameLogic implements EventHandler<ActionEvent> {

    private int numRows = 4;
    private int nuMCol = 4;
    private int[][] cells = new int[numRows][nuMCol];
    private Boolean gameStatus = false;
    private int totalMoves = 0;


    public void playGame()  {
        addListenersToAllImageViews();
        initializeCellsPosition();
    }

    private void initializeCellsPosition() {
        for(int row = 0; row < numRows; row++){
            for(int col = 0; col < nuMCol; col++){
                cells[row][col] = -1;
            }
        }
    }

    private void addListenersToAllImageViews() {
        for(int row = 0; row < numRows; row++){
            for(int col = 0; col < nuMCol; col++){
                GameBoard.gridImageView[row][col].addEventHandler(MouseEvent.MOUSE_CLICKED, new ImageClickHandler());
            }
        }

        GameBoard.newGameButton.setOnAction((event) -> {
            System.out.println("New Game button clicked");
            initializeCellsPosition();
            gameStatus = false;
            totalMoves = 0;

            GameBoard.changeLabel("X's turn");
            resetBoard();
        });

    }

    private void resetBoard() {
        for(int row = 0; row < numRows; row++){
            for(int col = 0; col < nuMCol; col++){
                GameBoard.gridImageView[row][col].setImage(new Image("imageFiles/square.png"));
            }
        }
    }

    class ImageClickHandler implements EventHandler<MouseEvent> {
        @Override
        public void handle(MouseEvent e){
            System.out.println("Source: " + e.getSource());
            if(!gameStatus){
                checkMove(e);
            }
            else{
                    GameBoard.changeLabel("Game Over - Click New Game to try again");
            }
            System.out.println("========img-clicked=======");
        }
    }

    private void checkMove(MouseEvent e){
        int currentRow;
        int currentCol;
        String xTurn = "X's turn";
        String oTurn = "0's turn";
        for(int row = 0; row < numRows; row++){
            for(int col = 0; col < nuMCol; col++){
                if(GameBoard.gridImageView[row][col] == (ImageView) e.getSource()){
                    currentCol = col;
                    currentRow = row;
                    System.out.println("match found at - " + "row: " + row + ",  col: " + col);

                    if(cells[currentRow][currentCol] == -1){
                        if(GameBoard.label.getText() == xTurn){
                            cells[currentRow][currentCol] = 1;
                            totalMoves++;
                            GameBoard.changeLabel(oTurn);
                            GameBoard.changeImage(xTurn,row,col);
                        }
                        else if (GameBoard.label.getText() == oTurn) {
                            cells[currentRow][currentCol] = 0;
                            totalMoves++;
                            GameBoard.changeLabel(xTurn);
                            GameBoard.changeImage(oTurn,row,col);
                        }
                        else {
                            gameStatus = true;
                        }

                    }
                    checkHorizontally(row,col);
                    checkVertically(row, col);
                    checkMainDiagonally(row,col);
                    checkSecondaryDiagonally(row,col);
                    System.out.println("total moves - " + totalMoves);

                    if(totalMoves == 16){
                        GameBoard.changeLabel("Draw :(");
                        gameStatus = true;
                    }
                    if(GameBoard.label.getText().contains("Won")){
                        gameStatus = true;
                    }
                }
            }
        }

    }

    private void checkHorizontally(int row, int col){
        int xCount=0;
        int oCount=0;
        System.out.println("Checking Horizontally");
        for(int colItem = 0; colItem < nuMCol; colItem++){
            System.out.println("x -" + row + ", col -" + colItem);
            if(cells[row][colItem] == 1){
                xCount++;
            }
            if(cells[row][colItem] == 0){
                oCount++;
            }
        }

        System.out.println("x score- " + xCount);
        System.out.println("0 score- " + oCount);

        if(xCount == 4){
            GameBoard.changeLabel("X Won");
        }
        if(oCount == 4){
            GameBoard.changeLabel("0 Won");
        }
    }
    private void checkVertically(int row, int col){
        int xCount=0;
        int oCount=0;
        System.out.println("Checking Vertically");
        for(int rowItem = 0; rowItem < numRows; rowItem++){
            System.out.println("x -" + rowItem + ", col -" + col);
            if(cells[rowItem][col] == 1){
                xCount++;
            }
            if(cells[rowItem][col] == 0){
                oCount++;
            }
        }

        System.out.println("x score- " + xCount);
        System.out.println("0 score- " + oCount);

        if(xCount == 4){
            GameBoard.changeLabel("X Won");
        }
        if(oCount == 4){
            GameBoard.changeLabel("0 Won");
        }
    }
    private void checkMainDiagonally(int row, int col){
        int xCount=0;
        int oCount=0;
        int rowItem = row;
        int colItem = col;
        System.out.println("Checking Main Diagonally");
        //Main Diagonal
        while (rowItem>=0 && colItem>=0){
            System.out.println("x -" + rowItem + ", col -" + colItem);
            if(cells[rowItem][colItem] == 1){
                xCount++;
            }
            if(cells[rowItem][colItem] == 0){
                oCount++;
            }
            rowItem--;
            colItem--;
        }

        rowItem = row+1;
        colItem = col+1;

        while (rowItem<4 && colItem<4){
            System.out.println("x -" + rowItem + ", col -" + colItem);
            if(cells[rowItem][colItem] == 1){
                xCount++;
            }
            if(cells[rowItem][colItem] == 0){
                oCount++;
            }
            rowItem++;
            colItem++;
        }

        System.out.println("x score- " + xCount);
        System.out.println("0 score- " + oCount);


        if(xCount == 4){
            GameBoard.changeLabel("X Won");
        }
        if(oCount == 4){
            GameBoard.changeLabel("0 Won");
        }
    }
    private void checkSecondaryDiagonally(int row, int col){
        int xCount=0;
        int oCount=0;
        int rowItem = row;
        int colItem = col;
        System.out.println("Checking Secondary Diagonally");

        while (rowItem>=0 && colItem<4){
            System.out.println("x -" + rowItem + ", col -" + colItem);
            if(cells[rowItem][colItem] == 1){
                xCount++;
            }
            if(cells[rowItem][colItem] == 0){
                oCount++;
            }
            rowItem--;
            colItem++;
        }

        rowItem = row+1;
        colItem = col-1;

        while (rowItem<4 && colItem>=0){
            System.out.println("x -" + rowItem + ", col -" + colItem);
            if(cells[rowItem][colItem] == 1){
                xCount++;
            }
            if(cells[rowItem][colItem] == 0){
                oCount++;
            }
            rowItem++;
            colItem--;
        }

        System.out.println("x score- " + xCount);
        System.out.println("0 score- " + oCount);


        if(xCount == 4){
            GameBoard.changeLabel("X Won");
        }
        if(oCount == 4){
            GameBoard.changeLabel("0 Won");
        }

    }


    @Override
    public void handle(ActionEvent actionEvent) {

    }
}
