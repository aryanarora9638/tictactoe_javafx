package ca.cmpt213.asn4.tictactoe.ui;

import ca.cmpt213.asn4.tictactoe.game.GameLogic;


/**
 * The main class calls the Gameboard and the GameLogin class where the former manages the UI for the game
 * and the latter manages the game logic and runs it.
 * @author aryanarora
 */


public class Main {
    public static void main(String[] args){
        GameBoard gameBoard = new GameBoard();
        gameBoard.mains(args);

        GameLogic gameLogic = new GameLogic();
        gameLogic.playGame();
    }
}
